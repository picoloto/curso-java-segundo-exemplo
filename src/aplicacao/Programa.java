package aplicacao;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entidades.Funcionario;

public class Programa {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);

		List<Funcionario> list = new ArrayList<Funcionario>();
		
		// PART 1 - Lendo as informa��es
		
		System.out.print("Quantos funcionarios ir� cadastrar? ");
		int n = sc.nextInt();
		
		for (int i=1; i<=n; i++) {
			System.out.println();
			System.out.println("Funcionario #" + i + ": ");
			System.out.print("Id: ");
			int id = sc.nextInt();
			System.out.print("Nome: ");
			sc.nextLine();
			String nome = sc.nextLine();
			System.out.print("Salario: ");
			double salario = sc.nextDouble();
			list.add(new Funcionario(id, nome, salario));
		}

		// PART 2 - Atualizando Salarios:
		
		System.out.println();
		System.out.print("Funcionario(ID) que ser� atualizado: ");
		int id = sc.nextInt();
		Funcionario emp = list.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
		if (emp == null) {
			System.out.println("Esse funcionario n�o existe!");
		}
		else {
			System.out.print("Porcentagem de aumento: ");
			double percentage = sc.nextDouble();
			emp.aumentaSalario(percentage);
		}
		
		// PART 3 - Lista funcionarios:
		
		System.out.println();
		System.out.println("Lista de Funcionarios:");
		for (Funcionario obj : list) {
			System.out.println(obj);
		}
				
		sc.close(); 
	}
}